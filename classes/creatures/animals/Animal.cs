namespace OOPLab5.creatures.animal;

public enum AnimalType {
	Flying,
	Swimming,
	Crawling,
	Walking
}

public abstract class Animal : Creature, IAnimal {
	private DateTime _birthDate;
	private bool     _isHumanFriendly = false;

	public string Species { get; }

	public TimeSpan Age => DateTime.Now - _birthDate;

	public bool WillBiteHumans {
		get => !_isHumanFriendly;
		init => _isHumanFriendly = !value;
	}

	public bool HasFur { get; init; } = false;

	public AnimalType Type { get; }

	public string[] ContinentsOfHabitation { get; init; } = { "Europe" };

	public Animal(string species, string sound, int legsCount, AnimalType type, DateTime? birthDate)
		: base(sound, 1, .5, birthDate) {
		Species = species;
		Type    = type;
	}
}

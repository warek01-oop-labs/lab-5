using OOPLab5.classes.root;

namespace OOPLab5.creatures;

public abstract class Creature : Movable, ICreature {
	private double _height;
	private double _weight;
	private string _sound;
	
	public double Height {
		get => _height;
		protected set {
			if (value < 0)
				throw new Exception("Height can't be negative");
			_height = value;
		}
	}
	
	public double Weight {
		get => _weight;
		protected set {
			if (value < 0)
				throw new Exception("Weight can't be negative");
			_weight = value;
		}
	}
	
	public DateTime BirthDate { get; protected set; }
	
	public Creature(string sound, double defaultWeight, double defaultHeight, DateTime? bdate) {
		BirthDate = bdate.GetValueOrDefault(DateTime.Now);
		Weight    = defaultHeight;
		Height    = defaultHeight;
		_sound    = sound;
	}
	
	public virtual void Say() {
		Console.WriteLine(_sound);
	}
}

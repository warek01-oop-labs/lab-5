using OOPLab5.classes.vehicles.Cars;
using OOPLab5.creatures.human;

namespace OOPLab5.classes.creatures.humans.driver;

public class Driver : Human, IDriver {
	private List<Car> _carsList;

	public List<Car> OwnedCars => _carsList;

	public Car CurrentCar { get; private set; }

	public Driver(string fname, string lname, DateTime bdate, List<Car> cars)
		: base(fname, lname, bdate) {
		_carsList = cars;
	}

	// public Driver(string fname, string lname, DateTime bdate, Car car) {
	// 	List<Car> cars = new();
	// 	cars.Add(car);
	// 	Driver(fname, lname, bdate, cars);
	// }

	public void SitInCar(Car car) {
		if (!_carsList.Contains(car)) {
			Console.WriteLine($"{FullName} does not own {car.CarInfo}");
			return;
		}
		
		CurrentCar = car;
	}
	
	public override void Say() {
		Console.WriteLine($"{FullName} says something about cars");
	}
}

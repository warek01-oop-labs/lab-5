using OOPLab5.classes.vehicles.Cars;

namespace OOPLab5.classes.creatures.humans.driver; 

public interface IDriver {
	public void SitInCar(Car car);
	public void Say();
}

namespace OOPLab5.classes.road_objects.traffic_lights; 

public interface ITrafficLight {
	public void Tick();
}

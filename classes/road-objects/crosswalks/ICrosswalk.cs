using OOPLab5.creatures.human;

namespace OOPLab5.classes.road_objects.crosswalks;

public interface ICrosswalk {
	List<Human> Peds { get; }

	int PedsCount { get; }

	bool IsBeingCrossed { get; }

	void StartCrossing();
	void StopCrossing();
	void Tick();
}

﻿namespace OOPLab5.classes.road_objects.crosswalks.uncontrolled_crosswalk; 

public interface IUncontrolledCrosswalk {
	void Tick();
}

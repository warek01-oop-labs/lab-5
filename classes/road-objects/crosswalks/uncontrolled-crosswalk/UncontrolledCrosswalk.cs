﻿using OOPLab5.classes.helpers;

namespace OOPLab5.classes.road_objects.crosswalks.uncontrolled_crosswalk;

public class UncontrolledCrosswalk : Crosswalk, IUncontrolledCrosswalk {
		protected int CrossTicksRemaining = 0;

	public UncontrolledCrosswalk(int min, int max) : base(min, max) { }

	public override void Tick() {
		int prevCount = PedsCount;
		
		base.Tick();

		int newCount = PedsCount;

		if (newCount > prevCount) {
			CrossTicksRemaining += Config.PedCrossingTicks;
			StartCrossing();
		}
		
		if (CrossTicksRemaining > 0) {
			CrossTicksRemaining--;
		}
		else {
			StopCrossing();
			Peds.Clear();
		}
	}
}

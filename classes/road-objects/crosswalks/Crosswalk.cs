using OOPLab5.classes.helpers;
using OOPLab5.creatures.human;

namespace OOPLab5.classes.road_objects.crosswalks;

public abstract class Crosswalk : RoadObject, ICrosswalk {
	protected int MinPedCooldown;
	protected int MaxPedCooldown;
	protected int Cooldown = 0;

	public List<Human> Peds { get; } = new();

	public int PedsCount => Peds.Count;

	public bool IsBeingCrossed { get; protected set; } = false;


	public Crosswalk(int min, int max) {
		MinPedCooldown = min;
		MaxPedCooldown = max;
	}

	public virtual void Tick() {
		if (Cooldown == 0) {
			Random rand = new();
			Peds.Add(Generate.Ped());
			Cooldown = rand.Next(MinPedCooldown, MaxPedCooldown);
		}
		else {
			Cooldown--;
		}
	}

	public virtual void StartCrossing() {
		IsBeingCrossed = true;
	}

	public virtual void StopCrossing() {
		IsBeingCrossed = false;
	}
}

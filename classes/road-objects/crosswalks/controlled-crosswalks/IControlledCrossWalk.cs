﻿using OOPLab5.classes.road_objects.traffic_lights;

namespace OOPLab5.classes.road_objects.crosswalks.controlled_crosswalks;

public interface IControlledCrossWalk : ICrosswalk {
	TrafficLight TrafficLight { get; }

	void Tick();
}

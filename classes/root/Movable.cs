namespace OOPLab5.classes.root; 

public class Movable: Entity, IMovable {
	public int X => base.X;
	public int Y => base.Y;

	protected void ChangePosition(int x, int y) {
		base.X = x;
		base.Y = y;
	}
}

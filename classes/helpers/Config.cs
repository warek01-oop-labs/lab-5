﻿using System.Text;
using System.Text.Json.Nodes;

namespace OOPLab5.classes.helpers;

public static class Config {
	private static JsonNode _config;

	public class Range {
		public int From;
		public int To;

		public Range(int from, int to) {
			From = from;
			To   = to;
		}
	}

	public class TrafficLightConfig {
		public int At;
		public int GreenDuration;
		public int RedDuration;

		public TrafficLightConfig(int at, int greenDuration, int redDuration) {
			At            = at;
			GreenDuration = greenDuration;
			RedDuration   = redDuration;
		}
	}

	public class UncontrolledCrossWalkConfig {
		public int At;
		public int MinPedCooldown;
		public int MaxPedCooldown;

		public UncontrolledCrossWalkConfig(int at, int min, int max) {
			At             = at;
			MinPedCooldown = min;
			MaxPedCooldown = max;
		}
	}

	public class ControlledCrossWalkConfig {
		public int At;
		public int MinPedCooldown;
		public int MaxPedCooldown;
		public int GreenTiming;
		public int RedTiming;

		public ControlledCrossWalkConfig(int at, int min, int max, int greenTiming, int redTiming) {
			At             = at;
			MinPedCooldown = min;
			MaxPedCooldown = max;
			GreenTiming    = greenTiming;
			RedTiming      = redTiming;
		}
	}

	public static bool IsDebug;
	public static bool IsInfinite;
	public static int  BandWidth;
	public static int  TicksCount;
	public static int  TickSleepMs;
	public static int  PedCrossingTicks;
	public static int  BandLength;

	public static Range View;
	public static Range CarFlowCooldown;

	public static List<TrafficLightConfig>          TrafficLights        = new();
	public static List<UncontrolledCrossWalkConfig> CrossWalks           = new();
	public static List<ControlledCrossWalkConfig>   ControlledCrossWalks = new();

	public static void Init() {
		string raw = File.ReadAllText("../../../config.json", Encoding.UTF8);

		if (raw is null or "") {
			throw new Exception("Could not read config file");
		}

		_config = JsonNode.Parse(raw)!;

		if (_config is null) {
			throw new Exception("Could not parse config file");
		}

		IsDebug          = (bool)_config["debug"]!;
		IsInfinite       = (bool)_config["infinite"]!;
		BandWidth        = (int)_config["bandWidth"]!;
		TicksCount       = (int)_config["ticksCount"]!;
		TickSleepMs      = (int)_config["tickSleepMs"]!;
		PedCrossingTicks = (int)_config["pedCrossingTicks"]!;
		BandLength       = (int)_config["bandLength"]!;

		View = new Range(
			(int)_config["view"]!["from"]!,
			(int)_config["view"]!["to"]!
		);

		CarFlowCooldown = new Range(
			(int)_config["carFlow"]!["minCooldown"]!,
			(int)_config["carFlow"]!["maxCooldown"]!
		);

		JsonArray lightsArray = _config["trafficLights"]!.AsArray();
		foreach (JsonNode? lightNode in lightsArray) {
			TrafficLightConfig light = new(
				(int)lightNode!["at"]!,
				(int)lightNode["greenDuration"]!,
				(int)lightNode["redDuration"]!
			);

			TrafficLights.Add(light);
		}

		JsonArray crossWalksArray = _config["uncontrolledCrosswalks"]!.AsArray();
		foreach (JsonNode? crossWalkNode in crossWalksArray) {
			UncontrolledCrossWalkConfig walk = new(
				(int)crossWalkNode!["at"]!,
				(int)crossWalkNode!["minPedCooldown"]!,
				(int)crossWalkNode!["maxPedCooldown"]!
			);

			CrossWalks.Add(walk);
		}

		JsonArray controlledCrossWalksArray = _config["controlledCrosswalks"]!.AsArray();
		foreach (JsonNode? controlledCrossWalkNode in controlledCrossWalksArray) {
			ControlledCrossWalkConfig walk = new(
				(int)controlledCrossWalkNode!["at"]!,
				(int)controlledCrossWalkNode!["minPedCooldown"]!,
				(int)controlledCrossWalkNode!["maxPedCooldown"]!,
				(int)controlledCrossWalkNode!["greenDuration"]!,
				(int)controlledCrossWalkNode!["redDuration"]!
			);

			ControlledCrossWalks.Add(walk);
		}
	}
}

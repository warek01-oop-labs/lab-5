﻿using OOPLab5.classes.roads.road_bands;

namespace OOPLab5.classes.helpers;

public static class Display {
	public static void Band(RoadBand band) {
		Console.Clear();

		for (int i = Config.View.From; i < Config.View.To; i++) {
			if (band.HasCrossWalkAt(i)) {
				var walk = band.Crosswalks[i]!;

				for (int j = 0; j < Config.BandWidth + 2; j++) {
					if (band.HasCarAt(i) && j == Config.BandWidth / 2 + 1) {
						_car();
						continue;
					}

					Console.BackgroundColor = j % 2 == 0
						? ConsoleColor.White
						: ConsoleColor.Black;

					Console.Write(" ");
					Console.ResetColor();
				}

				Console.Write($" {walk.PedsCount}");
			}
			else {
				Console.ResetColor();
				Console.Write("|");
				for (int j = 0; j < Config.BandWidth; j++) {
					if (band.HasCarAt(i) && j == Config.BandWidth / 2) {
						_car();
						Console.ResetColor();
						continue;
					}

					Console.Write(" ");
				}

				Console.Write("|");
			}

			if (i == Config.View.From) {
				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.Write("   Press ");

				Console.ForegroundColor = ConsoleColor.Red;
				Console.Write('Q');

				Console.ForegroundColor = ConsoleColor.DarkGreen;
				Console.Write(" to quit simulation.");
			}

			if (band.HasTrafficLightAt(i))
				_trafficLight(band.TrafficLights[i]!.IsGreen);

			Console.ResetColor();
			Console.WriteLine();
		}
	}

	private static void _trafficLight(bool isGreen) {
		Console.Write(" ");
		Console.BackgroundColor = isGreen ? ConsoleColor.Green : ConsoleColor.Red;
		Console.Write(" ");
		Console.ResetColor();
		Console.Write(" ");
	}

	private static void _car() {
		Console.BackgroundColor = ConsoleColor.DarkBlue;
		Console.ForegroundColor = ConsoleColor.Black;
		Console.Write("*");
		Console.ResetColor();
	}
}

using System.Text;
using OOPLab5.classes.road_objects.crosswalks.controlled_crosswalks;
using OOPLab5.classes.road_objects.crosswalks.uncontrolled_crosswalk;
using OOPLab5.classes.road_objects.traffic_lights;
using OOPLab5.classes.roads.road_bands;
using OOPLab5.classes.vehicles.Cars;

namespace OOPLab5.classes.helpers;

public static class Simulate {
	public static void CarFlow() {
		Random rand = new();

		RoadBand band = new(Config.BandLength);

		foreach (var light in Config.TrafficLights)
			band.AddLight(
				new TrafficLight(light.GreenDuration, light.RedDuration),
				light.At
			);

		foreach (var walk in Config.CrossWalks)
			band.AddCrosswalk(
				new UncontrolledCrosswalk(walk.MinPedCooldown, walk.MaxPedCooldown),
				walk.At
			);

		foreach (var walk in Config.ControlledCrossWalks) {
			TrafficLight light = new(walk.GreenTiming, walk.RedTiming);

			band.AddCrosswalk(
				new ControlledCrossWalk(light, walk.MinPedCooldown, walk.MaxPedCooldown),
				walk.At
			);

			band.AddLight(light, walk.At);
		}

		int    tick                = 0;
		int    carFlowCooldown     = 0;
		int    carsNotEnteredCount = 0;
		int    totalLeaveTime      = 0;
		int    totalCarsEnetred    = 0;
		int    totalLightsTime     = 0;
		int    totalCrosswalksTime = 0;
		int    totalWaitTime       = 0;
		double avgLeaveTime;
		double avgLightsTime;
		double avgCrosswalksTime;
		double avgWaitTime;

		while (true) {
			if (Console.KeyAvailable) {
				ConsoleKey key = Console.ReadKey(true).Key;
				if (key == ConsoleKey.Q) break;
			}

			if (carFlowCooldown == 0) {
				Car car = Generate.Car();
				car.MoveTo(Config.BandWidth / 2, car.Y);
				car.EnteredBandAt = tick;
				bool result = band.PushCar(car);

				if (result == false)
					carsNotEnteredCount++;

				totalCarsEnetred++;

				carFlowCooldown = rand.Next(
					Config.CarFlowCooldown.From,
					Config.CarFlowCooldown.To
				);
			}
			else {
				carFlowCooldown--;
			}

			band.Tick(
				ref tick,
				ref totalLeaveTime,
				ref totalLightsTime,
				ref totalCrosswalksTime,
				ref totalWaitTime
			);
			Display.Band(band);

			if (!Config.IsInfinite && tick == Config.TicksCount) {
				break;
			}

			if (!Config.IsDebug) {
				Thread.Sleep(Config.TickSleepMs);
			}
		}

		avgLeaveTime      = (double)totalLeaveTime / totalCarsEnetred;
		avgWaitTime       = (double)totalWaitTime  / totalCarsEnetred;
		avgLightsTime     = (double)totalWaitTime  / totalLightsTime;
		avgCrosswalksTime = (double)totalWaitTime  / totalCrosswalksTime;

		string resultStr = "";
		resultStr += $"Simulation ended after {tick} ticks\n";
		resultStr += $"Total generated cars: {totalCarsEnetred}\n";
		resultStr += $"Cars currently on band: {band.CarsOnBand}\n";
		resultStr += $"Cars exited band: {band.TotalCarFlow}\n";
		resultStr += $"Cars not entered on band due to jam: {carsNotEnteredCount}\n";
		resultStr += $"Average time on band: {avgLeaveTime:0.0}\n";
		resultStr += $"Average wait time: {avgWaitTime:0.0}\n";
		resultStr += $"Average time on red light: {avgLightsTime:0.0}\n";
		resultStr += $"Average time on crosswalks: {avgCrosswalksTime:0.0}\n";

		File.WriteAllText("results.txt", resultStr, Encoding.UTF8);
		
		Console.Clear();
		Console.ForegroundColor = ConsoleColor.DarkCyan;
		Console.Write(resultStr);
		Console.ResetColor();
		Console.WriteLine("\nPress any key to exit\n");
		Console.ReadKey(true);

	}
}

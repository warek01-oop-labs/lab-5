namespace OOPLab5.classes.vehicles.Cars;

public class Coupe : Car {
	public Coupe(string brandName) : base(2, "Coupe", brandName) { }

	public override void PresentCar() {
		Console.WriteLine("This is a nice car!");
		base.PresentCar();
	}
}

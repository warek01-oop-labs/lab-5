namespace OOPLab5.classes.vehicles.Cars;

public class Sedan : Car {
	public Sedan(string brandName) : base(4, "Sedan", brandName) { }
}

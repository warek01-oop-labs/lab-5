namespace OOPLab5.classes.vehicles.Cars;

public class Crossover : Car {
	public Crossover(string brandName) : base(4, "Crossover", brandName) { }
}

namespace OOPLab5.classes.vehicles.Cars;

public class Hatchback : Car {
	public Hatchback(string brandName, bool isShort) : base(isShort ? 2 : 4, "Hatchback", brandName) { }
}

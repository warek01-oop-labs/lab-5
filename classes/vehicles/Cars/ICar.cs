namespace OOPLab5.classes.vehicles.Cars;

public interface ICar {
	public int    MaxSpeed  { get; init; }
	public int    Height    { get; init; }
	public int    Width     { get; init; }
	public int    Clearance { get; init; }
	public int    Length    { get; init; }
	public string BrandName { get; }
	public string CarInfo   { get; }
	public string CarType   { get; }

	public void PresentCar();
}

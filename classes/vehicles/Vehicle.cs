using OOPLab5.classes.root;

namespace OOPLab5.classes.vehicles;

public abstract class Vehicle : Movable, IVehicle {
	private int    _doorsCount;
	private int    _wheelsCount;
	private string _type;
	private int    _weight;

	public int WaitedOnRedLight  { get; set; } = 0;
	public int WaitedOnCrosswalk { get; set; } = 0;
	public int Waited            { get; set; } = 0;
	public int EnteredBandAt     { get; set; } = 0;
	public int LeftBandAt        { get; set; } = 0;

	public int    DoorsCount  => _doorsCount;
	public int    WheelsCount => _wheelsCount;
	public string VehicleInfo => $"{_type} has {_doorsCount} doors and {_wheelsCount} wheels";

	public int Weight {
		get => _weight;
		init {
			if (value < 0)
				throw new Exception("Weight can't be negative");

			_weight = value;
		}
	}

	protected Vehicle(int doorsCount, int wheelsCount, string type, int defaultWeight) {
		if (doorsCount < 0)
			throw new Exception("Doors count can't be negative");

		if (wheelsCount < 0)
			throw new Exception("Wheels count can't be negative");

		if (defaultWeight < 0)
			throw new Exception("Weight can't be negative");

		_doorsCount  = doorsCount;
		_wheelsCount = wheelsCount;
		_type        = type;
		_weight      = defaultWeight;
	}

	public void MoveTo(int x, int y) {
		ChangePosition(x, y);
	}
}

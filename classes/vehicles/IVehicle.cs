namespace OOPLab5.classes.vehicles;

public interface IVehicle {
	int    DoorsCount  { get; }
	int    WheelsCount { get; }
	string VehicleInfo { get; }
	int    Weight      { get; init; }

	int        WaitedOnRedLight  { get; set; }
	int        WaitedOnCrosswalk { get; set; }
	int        Waited            { get; set; }
	public int EnteredBandAt     { get; set; }
	public int LeftBandAt        { get; set; }

	void MoveTo(int x, int y);
}

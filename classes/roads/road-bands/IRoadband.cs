using OOPLab5.classes.road_objects.crosswalks;
using OOPLab5.classes.road_objects.traffic_lights;
using OOPLab5.classes.vehicles.Cars;

namespace OOPLab5.classes.roads.road_bands;

public interface IRoadband {
	int TotalCarFlow { get; }
	int CarsOnBand   { get; }
	int Length       { get; }

	List<Car?>                 Cars                 { get; }
	List<TrafficLight?>        TrafficLights        { get; }
	List<Crosswalk>           Crosswalks           { get; }

	bool HasCarAt(int                 index);
	bool HasTrafficLightAt(int        index);
	bool HasCrossWalkAt(int           index);

	bool MoveCar(Car   car);
	bool PushCar(Car   car);
	bool RemoveCar(Car car);

	void AddLight(TrafficLight  light, int index);
	void AddCrosswalk(Crosswalk walk,  int index);

	void Tick(ref int tick);
}

using OOPLab5.classes.road_objects.crosswalks;
using OOPLab5.classes.road_objects.traffic_lights;
using OOPLab5.classes.root;
using OOPLab5.classes.vehicles.Cars;

namespace OOPLab5.classes.roads.road_bands;

public class RoadBand : Unmovable {
	public int TotalCarFlow { get; private set; }
	public int CarsOnBand   => Cars.Count(car => car != null);
	public int Length       { get; }

	public List<Car?>          Cars          { get; }
	public List<TrafficLight?> TrafficLights { get; }
	public List<Crosswalk?>    Crosswalks    { get; }

	public RoadBand(int length) {
		Length        = length;
		Cars          = new(new Car?[length]);
		TrafficLights = new(new TrafficLight?[length]);
		Crosswalks    = new(new Crosswalk?[length]);
	}

	public bool HasCarAt(int index) {
		return Cars[index] != null;
	}

	public bool HasTrafficLightAt(int index) {
		return TrafficLights[index] != null;
	}

	public bool HasCrossWalkAt(int index) {
		return Crosswalks[index] != null;
	}

	public bool MoveCar(Car car) {
		int index = Cars.FindIndex(c => c == car);
		if (index == -1) {
			return false;
		}

		Cars[index]     = null;
		Cars[index + 1] = car;
		car.MoveTo(car.X, index + 1);

		return true;
	}

	public bool PushCar(Car car) {
		if (Cars[0] != null) {
			return false;
		}

		car.MoveTo(0, 0);
		Cars[0] = car;

		return true;
	}

	public bool RemoveCar(Car car) {
		int index = Cars.FindIndex(c => car == c);

		if (index == -1) {
			return false;
		}

		Cars[index] = null;
		TotalCarFlow++;

		return true;
	}

	public void AddLight(TrafficLight light, int index) {
		if (TrafficLights[index] != null) {
			throw new Exception($"A light already placed at {index}");
		}

		TrafficLights[index] = light;
	}

	public void AddCrosswalk(Crosswalk walk, int index) {
		if (Crosswalks[index] != null) {
			throw new Exception($"A crosswalk already placed at {index}");
		}

		Crosswalks[index] = walk;
	}

	public void Tick(
		ref int tick,
		ref int totalLeaveTime,
		ref int totalLightsTime,
		ref int totalCrosswalksTime,
		ref int totalWaited
	) {
		for (int i = Cars.Count - 1; i >= 0; i--) {
			if (Cars[i] == null) continue;

			Car car = Cars[i]!;

			if (i >= Cars.Count - 1) {
				car.LeftBandAt      =  tick;
				totalLeaveTime      += car.LeftBandAt - car.EnteredBandAt;
				totalLightsTime     += car.WaitedOnRedLight;
				totalCrosswalksTime += car.WaitedOnCrosswalk;
				totalWaited         += car.Waited;
				RemoveCar(car);
				continue;
			}

			if (HasCarAt(i + 1)) {
				car.Waited++;
				continue;
			}

			if (HasTrafficLightAt(i + 1)) {
				TrafficLight light = TrafficLights[i + 1]!;

				if (light.IsRed) {
					car.Waited++;
					car.WaitedOnRedLight++;
					continue;
				}
			}

			if (
				HasCrossWalkAt(i + 1)
				&& Crosswalks[i + 1]!.IsBeingCrossed
			) {
				car.Waited++;
				car.WaitedOnCrosswalk++;
				continue;
			}

			MoveCar(car);
		}

		foreach (var light in TrafficLights) {
			light?.Tick();
		}

		foreach (var walk in Crosswalks) {
			walk?.Tick();
		}

		tick++;
	}
}
